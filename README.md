# Placeholder image server
This is a simple placeholder image server.
The user submits a `GET` request to the url
(host:port/image/width/height) and then they get back a <i>PNG</i>
image.

# Dependencies
- django
- Pillow (Image manipulation library)

# Usage
Clone this project to your local development box:

    $ cd [project directory]

    $ git clone git@bitbucket.org:lym/lwd_placeholder_image_server.git

Create a virtual environment, to isolate your project, with:

    virtualenv [path to you prefered python version] env

Download the dependencies with *pip3*

    $ pip install -r requirements.txt`

Start the server by the running the command:

    $ env/bin/python placeholder.py runserver`

 Start making test requests using a web browser, curl or any other
  http client.
